package com.acme.gateway_server.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FallbackController {

    @GetMapping("/userServiceFallback")
    public String userServiceFallback() {
        return "User Service is Down now!!";
    }

    @GetMapping("/indexerServiceFallback")
    public String indexerServiceFallback() {
        return "indexer Service is Down now!!";
    }
}
